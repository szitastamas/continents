-----------------------------------------------------------------------
-- 
-- Basisdaten der Mondial-DB wurden aus dem CIA-World Fact Book
-- entnommen. Die Original-Skripte f�r Oracle finden man unter:
-- http://www.dbis.informatik.uni-goettingen.de/Mondial/
--
-- (C) 2008 Mondial-DB for SQL Server 2000/2005 - Eduard Paul
-----------------------------------------------------------------------

IF not EXISTS (
  SELECT * 
    FROM sys.databases 
   WHERE name = N'Mondial'
)
CREATE DATABASE [Mondial]
GO

Use [Mondial]
GO


IF OBJECT_ID('FK_CITY_IN_COUNTRY') IS NOT NULL
	ALTER TABLE [city] DROP CONSTRAINT [FK_CITY_IN_COUNTRY] 
GO
IF OBJECT_ID('FK_COUNTRY_CAPITAL') IS NOT NULL
	ALTER TABLE [country] DROP CONSTRAINT [FK_COUNTRY_CAPITAL] 
GO
IF OBJECT_ID('FK_PROVINCE_CAPITAL') IS NOT NULL
	ALTER TABLE [province] DROP CONSTRAINT [FK_PROVINCE_CAPITAL]
GO
/*
	ALTER TABLE [economy] DROP CONSTRAINT [FK_ECONOMYDATA_FOR_COUNTRY]
	ALTER TABLE [population] DROP CONSTRAINT [FK_POPLATIONDATA_FOR_COUNTRY]
	ALTER TABLE [politics] DROP CONSTRAINT [FK_POLITICSDATA_FOR_COUNTRY] 
	ALTER TABLE [language] DROP CONSTRAINT [FK_LANGUAGEDATA_FOR_COUNTRY]
	ALTER TABLE [religion] DROP CONSTRAINT [FK_RELIGIONDATA_FOR_COUNTRY]
	ALTER TABLE [ethnic_group] DROP CONSTRAINT [FK_ETHNICITYDATA_FOR_COUNTRY]
	ALTER TABLE [borders] DROP CONSTRAINT [FK_BORDER_WITH_COUNTRY_1]  
	ALTER TABLE [borders] DROP CONSTRAINT [FK_BORDER_WITH_COUNTRY_2] 
	ALTER TABLE [encompasses] DROP CONSTRAINT [FK_CONTINENT]
	ALTER TABLE [encompasses] DROP CONSTRAINT [FK_COUNTRY_TAKES_UP_AREA_OF_CONTINENT]
[FK_ORGANIZATION_IN_CITY]
[FK_MEMBER_COUNTRY]
[FK_WHICH_ORGANIZATION]
[FK_FLOW_INTO_RIVER] 
[FK_LAKE]
[FK_SEA]
[FK_MOUNTAIN_PROVINCE]
[FK_DESERT_PROVINCE]
[FK_ISLAND_PROVINCE]
[FK_RIVER_PROVINCE] 
[FK_SEA_PROVINCE] 
[FK_LAKE_PROVINCE]
[FK_MERGE_SEA1]
[FK_MERGE_SEA2]
[FK_CITY_LOCATED_AT_SEA] 
[FK_CITY_LOCATED_AT_RIVER] 
[FK_CITY_LOCATED_AT_LAKE] 
[FK_CITY_LOCATION] 
*/
GO


-----------------------------------------------------
-- DROP old versions of existing tables
-----------------------------------------------------

IF OBJECT_ID('geo_mountain', 'U') IS NOT NULL
  DROP TABLE [geo_mountain]
GO

IF OBJECT_ID('geo_desert', 'U') IS NOT NULL
  DROP TABLE [geo_desert]
GO

IF OBJECT_ID('geo_island', 'U') IS NOT NULL
  DROP TABLE [geo_island]
GO

IF OBJECT_ID('geo_river', 'U') IS NOT NULL
  DROP TABLE [geo_river]
GO

IF OBJECT_ID('geo_sea', 'U') IS NOT NULL
  DROP TABLE [geo_sea]
GO

IF OBJECT_ID('geo_lake', 'U') IS NOT NULL
  DROP TABLE [geo_lake]
GO

IF OBJECT_ID('encompasses', 'U') IS NOT NULL
  DROP TABLE [encompasses]
GO

IF OBJECT_ID('merges_with', 'U') IS NOT NULL
  DROP TABLE [merges_with]
GO

IF OBJECT_ID('located', 'U') IS NOT NULL
  DROP TABLE [located]
GO

IF OBJECT_ID('economy', 'U') IS NOT NULL
  DROP TABLE [economy]
GO
IF OBJECT_ID('population', 'U') IS NOT NULL
  DROP TABLE [population]
GO

IF OBJECT_ID('politics', 'U') IS NOT NULL
  DROP TABLE [politics]
GO
IF OBJECT_ID('religion', 'U') IS NOT NULL
  DROP TABLE [religion]
GO

IF OBJECT_ID('ethnic_group', 'U') IS NOT NULL
  DROP TABLE [ethnic_group]
GO

IF OBJECT_ID('language', 'U') IS NOT NULL
  DROP TABLE [language]
GO

IF OBJECT_ID('borders', 'U') IS NOT NULL
  DROP TABLE [borders]
GO

IF OBJECT_ID('is_member', 'U') IS NOT NULL
  DROP TABLE [is_member]
GO

IF OBJECT_ID('continent', 'U') IS NOT NULL
  DROP TABLE [continent]
GO

IF OBJECT_ID('mountain', 'U') IS NOT NULL
  DROP TABLE [mountain]
GO

IF OBJECT_ID('river', 'U') IS NOT NULL
  DROP TABLE [river]
GO

IF OBJECT_ID('desert', 'U') IS NOT NULL
  DROP TABLE [desert]
GO

IF OBJECT_ID('island', 'U') IS NOT NULL
  DROP TABLE [island]
GO


IF OBJECT_ID('lake', 'U') IS NOT NULL
  DROP TABLE [lake]
GO

IF OBJECT_ID('organization', 'U') IS NOT NULL
  DROP TABLE [organization]
GO

IF OBJECT_ID('sea', 'U') IS NOT NULL
  DROP TABLE [sea]
GO

IF OBJECT_ID('province', 'U') IS NOT NULL
  DROP TABLE [province]
GO

IF OBJECT_ID('city', 'U') IS NOT NULL
  DROP TABLE [city]
GO

IF OBJECT_ID('country', 'U') IS NOT NULL
  DROP TABLE [country]
GO

-----------------------------------------------------------

CREATE TABLE [country]
(
	[Name] VARCHAR(32) NOT NULL UNIQUE,
	[Code] VARCHAR(4),
	[Capital] VARCHAR(35),
	[Province] VARCHAR(32),
	[Area] INT 
		CONSTRAINT C_AREA_NOT_ZERO 
		CHECK (Area >= 0),
	[Population] INT 
			CONSTRAINT C_POPULATION_ZERO_OR_MORE 
			CHECK (Population >= 0),
	CONSTRAINT PK_COUNTRY PRIMARY KEY (Code)
)
GO

CREATE TABLE [city]
(
	[Name] VARCHAR(35),
	[Country] VARCHAR(4),
	[Province] VARCHAR(32),
	[Population] INT 
		CONSTRAINT C_POPULATION_ZERO_OR_MORE_CITY 
		CHECK (Population >= 0),
	[Longitude] FLOAT 
		CONSTRAINT C_Longitude
		CHECK ((Longitude >= -180) AND (Longitude <= 180)) ,
	[Latitude] FLOAT 
		CONSTRAINT C_Latitude
		CHECK ((Latitude >= -90) AND (Latitude <= 90)) ,
	CONSTRAINT PK_CITY 
		PRIMARY KEY ([Name], Country, Province)
)
GO

CREATE TABLE [province]
(
	[Name] VARCHAR(32) NOT NULL,
	[Country]  VARCHAR(4) NOT NULL,
	[Population] INT 
		CONSTRAINT C_POPULATION_ZERO_OR_MORE_PROVINCE
		CHECK (Population >= 0),
	[Area] INT 
		CONSTRAINT C_Area_ZERO_OR_MORE
		CHECK (Area >= 0),
	[Capital] VARCHAR(35),
	[CapProv] VARCHAR(32),
	CONSTRAINT PK_PROVINCE PRIMARY KEY ([Name], Country)
)
GO

CREATE TABLE [economy]
(
	[Country] VARCHAR(4),
	[GDP] INT 
		CONSTRAINT GDP_ZERO_OR_MORE
		CHECK (GDP >= 0),
	[Agriculture] FLOAT,
	[Service] FLOAT,
	[Industry] FLOAT,
	[Inflation] FLOAT,
	CONSTRAINT PK_ECONOMY PRIMARY KEY (Country)
)
GO

CREATE TABLE [population]
(
	[Country] VARCHAR(4),
	[Population_Growth] FLOAT,
	[Infant_Mortality] FLOAT,
	CONSTRAINT PK_POPULATION PRIMARY KEY (Country)
)
GO


CREATE TABLE [politics]
(
	[Country] VARCHAR(4),
	[Independence] VARCHAR(15),
	[Government] VARCHAR(120),
	CONSTRAINT PK_POLITICS 
		PRIMARY KEY (Country)
)
GO

CREATE TABLE [language]
(
	[Country] VARCHAR(4),
	[Name] VARCHAR(50),
	[Percentage] FLOAT
		CONSTRAINT C_PERCENTAGE_WITHIN_RANGE_LANG
		CHECK ((Percentage > 0) AND (Percentage <= 100)),
	CONSTRAINT 
		PK_LANGUAGE PRIMARY KEY ([Name], Country)
)
GO

CREATE TABLE [religion]
(
	[Country] VARCHAR(4),
	[Name] VARCHAR(50),
	[Percentage] FLOAT
		CONSTRAINT C_PERCENTAGE_WITHIN_RANGE 
		CHECK ((Percentage > 0) AND (Percentage <= 100)),
	CONSTRAINT 
		PK_RELIGION PRIMARY KEY ([Name], Country)
)
GO

CREATE TABLE [ethnic_group]
(
	[Country] VARCHAR(4),
	[Name] VARCHAR(50),
	[Percentage] FLOAT
		CONSTRAINT C_PERCENTAGE_WITHIN_RANGE_ETHNIC_GROUP 
		CHECK ((Percentage > 0) AND (Percentage <= 100)),
	CONSTRAINT PK_ETHNICITY 
			PRIMARY KEY ([Name], Country)
)
GO

CREATE TABLE [continent]
(
	[Name] VARCHAR(20),
	[Area] INT,
	CONSTRAINT PK_CONTINENT PRIMARY KEY ([Name])
)
GO

CREATE TABLE [borders]
(
	[Country1] VARCHAR(4),
	[Country2] VARCHAR(4),
	[Length] INT
		CONSTRAINT C_LENGTH_NOT_ZERO 
		CHECK (Length > 0),
	CONSTRAINT 
		PK_BORDER PRIMARY KEY (Country1,Country2) 
)
GO

CREATE TABLE [encompasses]
(
	[Country] VARCHAR(4) NOT NULL,
	[Continent] VARCHAR(20) NOT NULL,
	[Percentage] FLOAT
		CONSTRAINT C_PERCENTAGE_WITHIN_RANGE_ENC 
		CHECK ((Percentage > 0) AND (Percentage <= 100)),
	CONSTRAINT 
		PK_ENCOMPASES PRIMARY KEY (Country,Continent)
)
GO

CREATE TABLE [organization]
(
	[Abbreviation] VARCHAR(12) PRIMARY KEY,
	[Name] VARCHAR(80) NOT NULL,
	[City] VARCHAR(35) ,
	[Country] VARCHAR(4) , 
	[Province] VARCHAR(32) ,
	[Established] DATETIME,
	CONSTRAINT C_ORG_HAS_UNIQUE_NAME UNIQUE ([Name])
)
GO

CREATE TABLE [is_member]
(
	[Country] VARCHAR(4),
	[Organization] VARCHAR(12),
	[Type] VARCHAR(30) DEFAULT 'member',
	CONSTRAINT PK_MEMBER
	PRIMARY KEY (Country,Organization)
)
GO

CREATE TABLE [mountain]
(	
	[Name] VARCHAR(20),
	[Height] INT CHECK (Height >= 0),
	[Longitude] FLOAT 
		CHECK ((Longitude >= -180) AND (Longitude <= 180)),
	[Latitude] FLOAT CHECK ((Latitude >= -90) AND (Latitude <= 90)),
	CONSTRAINT 
		PK_MOUNTAIN PRIMARY KEY (Name)
)
GO

CREATE TABLE [desert]
(
	[Name] VARCHAR(25),
	[Area] INT,
	CONSTRAINT PK_DESERT
		PRIMARY KEY (Name)
)
GO

CREATE TABLE [island]
(
	[Name] VARCHAR(25),
	[Islands] VARCHAR(25),
	[Area] INT CHECK ((Area >= 0) and (Area <= 2175600)) ,
	[Longitude] FLOAT CHECK ((Longitude >= -180) AND 
		            (Longitude <= 180)),
	[Latitude] FLOAT CHECK ((Latitude >= -90) AND
                       (Latitude <= 90)),
	CONSTRAINT 
		PK_ISLAND PRIMARY KEY ([Name])
)
GO

CREATE TABLE [lake]
(
	[Name] VARCHAR(25),
	[Area] INT 
		CHECK (Area >= 0),
	CONSTRAINT 
		PK_LAKE PRIMARY KEY ([Name])
)
GO

CREATE TABLE [sea]
(
	[Name] VARCHAR(25),
	[Depth] INT 
		CHECK (Depth >= 0),
	CONSTRAINT [SeaKey] PRIMARY KEY ([Name])
)
GO

CREATE TABLE [river]
(
	[Name] VARCHAR(20),
	River VARCHAR(20),
	Lake VARCHAR(25),
	Sea VARCHAR(25),
	Length INT CHECK (Length >= 0),
	CONSTRAINT RiverKey PRIMARY KEY (Name)
)
GO

CREATE TABLE [geo_mountain]
(
	[Mountain] VARCHAR(20) ,
	[Country] VARCHAR(4) ,
	[Province] VARCHAR(32) ,
	CONSTRAINT 
		PK_GEO_MOUNTAIN 
	PRIMARY KEY (Province,Country,Mountain) 
)
GO

CREATE TABLE [geo_desert]
(
	[Desert] VARCHAR(25) ,
	[Country] VARCHAR(4) ,
	[Province] VARCHAR(32) ,
	CONSTRAINT 
		PK_GEO_DESERT 
	PRIMARY KEY ([Province], [Country], [Desert])
)
GO

CREATE TABLE [geo_island]
(
	[Island] VARCHAR(25) , 
	[Country] VARCHAR(4) ,
	[Province] VARCHAR(32) ,
	CONSTRAINT PK_GEO_ISLAND 
	PRIMARY KEY ([Province], [Country], [Island]) 
)
GO

CREATE TABLE [geo_river]
(
	River VARCHAR(20) , 
	Country VARCHAR(4) ,
	Province VARCHAR(32) ,
	CONSTRAINT 
		PK_GEO_RIVER PRIMARY KEY ([Province] ,[Country], [River]) 
)
GO

CREATE TABLE [geo_sea]
(
	[Sea] VARCHAR(25) ,
	[Country] VARCHAR(4)  ,
	[Province] VARCHAR(32) ,
	CONSTRAINT PK_GEO_SEA 
	PRIMARY KEY ([Province], [Country], [Sea]) 
)
GO

CREATE TABLE [geo_lake]
(
	[Lake] VARCHAR(25) ,
	[Country] VARCHAR(4) ,
	[Province] VARCHAR(32) ,
	CONSTRAINT 
		PK_GEO_LAKE 
	PRIMARY KEY ([Province], [Country], [Lake]) 
)
GO


CREATE TABLE [merges_with]
(
	[Sea1] VARCHAR(25) ,
	[Sea2] VARCHAR(25) ,
	CONSTRAINT 
		PK_MERGES_WITH_SEA PRIMARY KEY ([Sea1],[Sea2]) 
)
GO


CREATE TABLE [located]
(
	[City] VARCHAR(35) ,
	[Province] VARCHAR(32) ,
	[Country] VARCHAR(4) ,
	[River] VARCHAR(20),
	[Lake] VARCHAR(25),
	[Sea] VARCHAR(25)
)
GO
