﻿using System;
using System.Collections.Generic;

namespace Caching_Continents.Models
{
    public partial class Continent
    {
        public Continent()
        {
            Encompasses = new HashSet<Encompasses>();
        }

        public string Name { get; set; }
        public int? Area { get; set; }

        public virtual ICollection<Encompasses> Encompasses { get; set; }
    }
}
