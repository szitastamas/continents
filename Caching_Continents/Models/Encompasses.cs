﻿using System;
using System.Collections.Generic;

namespace Caching_Continents.Models
{
    public partial class Encompasses
    {
        public string Country { get; set; }
        public string Continent { get; set; }
        public double? Percentage { get; set; }

        public virtual Continent ContinentNavigation { get; set; }
        public virtual Country CountryNavigation { get; set; }
    }
}
