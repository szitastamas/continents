﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Caching_Continents.Models
{
    public partial class MondialContext : DbContext
    {
        public MondialContext()
        {
        }

        public MondialContext(DbContextOptions<MondialContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Continent> Continent { get; set; }
        public virtual DbSet<Country> Country { get; set; }
        public virtual DbSet<Encompasses> Encompasses { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer("Server=.;Database=Mondial;Trusted_Connection=True;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("ProductVersion", "2.2.6-servicing-10079");

            modelBuilder.Entity<Continent>(entity =>
            {
                entity.HasKey(e => e.Name)
                    .HasName("PK_CONTINENT");

                entity.ToTable("continent");

                entity.Property(e => e.Name)
                    .HasMaxLength(20)
                    .IsUnicode(false)
                    .ValueGeneratedNever();
            });

            modelBuilder.Entity<Country>(entity =>
            {
                entity.HasKey(e => e.Code)
                    .HasName("PK_COUNTRY");

                entity.ToTable("country");

                entity.HasIndex(e => e.Name)
                    .HasName("UQ__country__737584F635AF8CF2")
                    .IsUnique();

                entity.Property(e => e.Code)
                    .HasMaxLength(4)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.Capital)
                    .HasMaxLength(35)
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(32)
                    .IsUnicode(false);

                entity.Property(e => e.Province)
                    .HasMaxLength(32)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Encompasses>(entity =>
            {
                entity.HasKey(e => new { e.Country, e.Continent })
                    .HasName("PK_ENCOMPASES");

                entity.ToTable("encompasses");

                entity.Property(e => e.Country)
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.Continent)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.HasOne(d => d.ContinentNavigation)
                    .WithMany(p => p.Encompasses)
                    .HasForeignKey(d => d.Continent)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_CONTINENT");

                entity.HasOne(d => d.CountryNavigation)
                    .WithMany(p => p.Encompasses)
                    .HasForeignKey(d => d.Country)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_COUNTRY_TAKES_UP_AREA_OF_CONTINENT");
            });
        }
    }
}
