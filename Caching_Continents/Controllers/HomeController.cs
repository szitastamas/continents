﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Caching_Continents.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Caching.Memory;

namespace Caching_Continents.Controllers
{
    public class HomeController : Controller
    {
        private MondialContext _context;

        public HomeController(MondialContext context)
        {
            _context = context;
        }

        public IActionResult Index()
        {
            
            List<Continent>  allContinents = _context.Continent.ToList();

            ViewBag.Continent = new SelectList(allContinents, "Name", "Name");

            return View();
        }

        public IActionResult ShowCountries(string Continent)
        {

            var countries = from country in _context.Country
                            join encomp in _context.Encompasses
                            on country.Code equals encomp.Country
                            where encomp.Continent == Continent
                            orderby country.Population ascending
                            select country;

            ViewBag.Vary = Continent;

            return PartialView("_ResultPartial", countries.ToList());
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
